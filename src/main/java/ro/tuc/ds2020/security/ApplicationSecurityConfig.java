//package ro.tuc.ds2020.security;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//
///**
// * This class is used to provide security to the endpoints.
// * Controllers can also provide security  at each endpoint
// * (Please check the annotations @HasRole and @HasAnyRoles).
// */
//@Configuration
//@EnableWebMvc
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    private AuthenticationSuccessHandler authenticationSuccessHandler;
//
//
//    /**
//     * Please read the following links to understand some key concepts:
//     * https://www.baeldung.com/java-config-spring-security
//     * https://www.programcreek.com/java-api-examples/?api=org.springframework.security.config.annotation.web.builders.HttpSecurity
//     *
//     */
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
////        http.csrf().disable()
////                .authorizeRequests().antMatchers("/register/**").permitAll()
////                .and().formLogin().loginPage("/login")
////                .successHandler(authenticationSuccessHandler)
////                //.defaultSuccessUrl("/")
////                .failureUrl("/login?error=true").permitAll()
////                .and().logout().permitAll()
////                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
////                .logoutSuccessUrl("/login")
////                .and().authorizeRequests().antMatchers("/doctor/**").hasRole("DOCTOR")
////                .and().authorizeRequests().antMatchers("/person/**").hasRole("PATIENT")
////                .and().authorizeRequests().antMatchers("/caregiver/**").hasRole("CAREGIVER")
////                .and().authorizeRequests().anyRequest().authenticated();
//        /**
//         * Comment the above part and enable this one to disable security.
//         * It is easier for development and testing.
//         */
//          http.authorizeRequests().anyRequest().permitAll();
//    }
//
//    @Bean
//    public AuthenticationManager customAuthenticationManager() throws Exception {
//        return authenticationManager();
//    }
//    @Autowired
//    public ApplicationSecurityConfig(AuthenticationSuccessHandler authenticationSuccessHandler) {
//        this.authenticationSuccessHandler = authenticationSuccessHandler;
//    }
//
//}