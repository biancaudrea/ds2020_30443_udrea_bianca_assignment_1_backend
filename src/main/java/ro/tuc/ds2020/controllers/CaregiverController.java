package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDto;
import ro.tuc.ds2020.services.CaregiverService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {
    @Autowired
    private CaregiverService caregiverService;

    @GetMapping(value = "/{username}")
    public ResponseEntity<List<PatientDto>> getPatients(@PathVariable String username) {


        List<PatientDto> dtos = caregiverService.getAllPatients(username);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
