package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedPlanDto;
import ro.tuc.ds2020.dtos.PatientDto;
import ro.tuc.ds2020.dtos.PatientInfoDto;
import ro.tuc.ds2020.services.PatientService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {
    @Autowired
    private PatientService patientService;

    @GetMapping(value = "/{username}")
    public ResponseEntity<List<PatientInfoDto>> getPatients(@PathVariable String username) {


        List<PatientInfoDto> dtos = patientService.getPatient(username);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @GetMapping(value = "/{username}/medicationplan")
    public ResponseEntity<List<MedPlanDto>> getMedPlanDtos(@PathVariable String username) {


        List<MedPlanDto> dtos = patientService.getAllMedPlanDtos(username);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
