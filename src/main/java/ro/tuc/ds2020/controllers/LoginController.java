package ro.tuc.ds2020.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ro.tuc.ds2020.dtos.LoggedInUserDto;
import ro.tuc.ds2020.dtos.LoginDto;
import ro.tuc.ds2020.dtos.UserLoginDto;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.PatientService;
import ro.tuc.ds2020.enums.GenderEnum;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * Here add the basic security endpoints:login, register, default redirect if login success
 */
@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private CaregiverService caregiverService;

    @GetMapping(value = "/")
    public ResponseEntity<?> getPatients() {
       if(doctorService.findByUsername("popalexandru")==null) {
            Doctor doctor = new Doctor("Pop Alexandru", "Baritiu no 12", "01.10.1980", GenderEnum.male, "popalexandru");
            doctorService.saveDoctor(doctor);
        }
        return new ResponseEntity<>( HttpStatus.OK);
    }
    @PostMapping(value = "/login")
    public ResponseEntity<LoggedInUserDto> insertPatient(@Valid @RequestBody LoginDto loginDto) {
       // String patientId = patientService.insert(patientDetailsDto,caregiverService.findByName(patientDetailsDto.getCaregiver()));
        boolean userFound = false;
        boolean corectCredentialsForDoctor = false;
        boolean corectCredentialsForPatient = false;
        boolean corectCredentialsForCaregiver = false;
        Doctor doctor=null;
        Patient patient=null;
        Caregiver caregiver=null;
        try{
            doctor = doctorService.findByUsername(loginDto.getUsername());
            if(doctor!=null){
                userFound = true;

                if(doctor.getPassword().equals(loginDto.getPassword()))
                {
                    corectCredentialsForDoctor = true;
                }
            }
        }catch (Exception e){

        }

        if(userFound == false){
            try{
                patient = patientService.findByUsername(loginDto.getUsername());
                if(patient!=null){
                    userFound = true;

                    if(patient.getPassword().equals(loginDto.getPassword()))
                    {
                        corectCredentialsForPatient = true;
                    }
                }
            }catch (Exception e){

            }
        }

        if(userFound == false){
            try{
                caregiver = caregiverService.findByUsername(loginDto.getUsername());
                if(caregiver!=null){
                    userFound = true;

                    if(caregiver.getPassword().equals(loginDto.getPassword()))
                    {
                        corectCredentialsForCaregiver = true;
                    }
                }
            }catch (Exception e){

            }
        }

        if(corectCredentialsForDoctor == true)
            return new ResponseEntity<>(new LoggedInUserDto(doctor.getUsername(),doctor.getId(),"doctor"), HttpStatus.OK);
        else  if(corectCredentialsForPatient == true)
            return new ResponseEntity<>(new LoggedInUserDto(patient.getUsername(),patient.getId(),"patient"), HttpStatus.OK);
        else  if(corectCredentialsForCaregiver == true)
            return new ResponseEntity<>(new LoggedInUserDto(caregiver.getUsername(),caregiver.getId(),"caregiver"), HttpStatus.OK);
        else
            return new ResponseEntity<>(new LoggedInUserDto("","",""), HttpStatus.OK);
    }

}