package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.enums.GenderEnum;
import ro.tuc.ds2020.services.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    @Autowired
    private MedicationService medicationService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private MedicationPlanService medicationPlanService;

    private final DoctorService doctorService;


    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/{username}")
    public ResponseEntity<List<PatientDto>> getPatients(@PathVariable String username) {


        List<PatientDto> dtos = doctorService.getAllPatients(username);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @PostMapping(value = "/{username}")
    public ResponseEntity<UUID> insertPatient(@Valid @RequestBody PatientDetailsDto patientDetailsDto) {
        String patientId = patientService.insert(patientDetailsDto,caregiverService.findByName(patientDetailsDto.getCaregiver()));
        return new ResponseEntity<>(UUID.fromString(patientId), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{username}/updatepatient")
    public ResponseEntity<UUID> updatePatient(@Valid @RequestBody PatientUpdateDto patientUpdateDto) {
        String patientId = patientService.updateMedicalRecord(patientUpdateDto);
        return new ResponseEntity<>(UUID.fromString(patientId), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{username}/deletepatient")
    public ResponseEntity<UUID> deletePatient(@Valid @RequestBody DeletePatientDto deleteUpdateDto) {
        String patientId = patientService.deletePatient(deleteUpdateDto);
        return new ResponseEntity<>(UUID.fromString(patientId), HttpStatus.OK);
    }

    @GetMapping(value = "/{username}/caregivers")
    public ResponseEntity<List<CaregiverDto>> getCaregivers(@PathVariable String username) {

        List<CaregiverDto> dtos = doctorService.getAllCaregiversOfDoctor(doctorService.findByUsername(username));

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/{username}/caregivers")
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDetailsDto caregiverDetailsDto,@PathVariable String username) {
        String caregiverId=caregiverService.insert(caregiverDetailsDto,doctorService.findByUsername(username));
        return new ResponseEntity<>(UUID.fromString(caregiverId), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{username}/caregivers/updatecaregiver")
    public ResponseEntity<UUID> updateCaregiver(@Valid @RequestBody CaregiverUpdateDto caregiverUpdateDto) {
        String caregiverId = caregiverService.updateAddress(caregiverUpdateDto);
        return new ResponseEntity<>(UUID.fromString(caregiverId), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{username}/caregivers/deletecaregiver")
    public ResponseEntity<UUID> deleteCaregiver(@Valid @RequestBody DeleteCaregiverDto deleteCaregiverDto) {
        String caregiverId = caregiverService.deleteCaregiver(deleteCaregiverDto);

        return new ResponseEntity<>(UUID.fromString(caregiverId), HttpStatus.OK);
    }

    @GetMapping(value = "/{username}/medication")
    public ResponseEntity<List<MedicationDto>> getMedication(@PathVariable String username) {

        List<MedicationDto> dtos = medicationService.getAllMedication();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/{username}/medication")
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody MedicationDetailsDto medicationDetailsDto,@PathVariable String username) {
        String medicationId= medicationService.insert(medicationDetailsDto);
        return new ResponseEntity<>(UUID.fromString(medicationId), HttpStatus.CREATED);
    }

    @PutMapping(value = "/{username}/updatemedication")
    public ResponseEntity<UUID> updateMedication(@Valid @RequestBody MedicationUpdateDto medicationUpdateDto) {
        String medicationId=medicationService.updateMedication(medicationUpdateDto);
        return new ResponseEntity<>(UUID.fromString(medicationId), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{username}/deletemedication")
    public ResponseEntity<UUID> deleteMedication(@Valid @RequestBody DeleteMedicationDto deleteMedicationDto) {
        String medicationId = medicationService.deleteMedication(deleteMedicationDto);

        return new ResponseEntity<>(UUID.fromString(medicationId), HttpStatus.OK);
    }

    @GetMapping(value = "/{username}/medicationplan")
    public ResponseEntity<List<MedicationPlanDto>> getMedicationPlan(@PathVariable String username) {

        List<MedicationPlanDto> dtos = medicationPlanService.getAllMedicationPlanDtos();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping(value = "/{username}/medicationplan")
    public ResponseEntity<UUID> insertMedicationPlan(@Valid @RequestBody MedicationPlanDto medicationPlanDto,@PathVariable String username) {
        String medicationPlanId = medicationPlanService.insert(medicationPlanDto);
        return new ResponseEntity<>(UUID.fromString(medicationPlanId), HttpStatus.CREATED);
    }
}
