package ro.tuc.ds2020.rmi;


import java.io.Serializable;

public class MedicationPlanSerializable implements Serializable {
    private String id;
    private String medicationName;
    private int intakeIntervals;
    private int period;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public int getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(int intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public MedicationPlanSerializable(String id, String medicationName, int intakeIntervals, int period) {
        this.id = id;
        this.medicationName = medicationName;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
    }

    public MedicationPlanSerializable() {
    }
}