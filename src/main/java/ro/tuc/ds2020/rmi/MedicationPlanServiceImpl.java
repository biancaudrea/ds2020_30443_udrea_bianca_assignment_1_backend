package ro.tuc.ds2020.rmi;


import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.services.MedicationPlanService;

import java.util.List;

@Service
public class MedicationPlanServiceImpl implements MedicationPlanServiceRmi {

    public final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanServiceImpl(MedicationPlanService medicationPlanService){
        this.medicationPlanService = medicationPlanService;
    }

    @Override
    public String getMedicationPlan(String patientId) throws Exception {
       // List<MedicationPlan> medicationPlans = medicationPlanService.getAllMedicationPlans();
        MedicationPlanSerializable medicationPlanSerializable = medicationPlanService.getMedicationPlanForPatient(patientId);
        if(medicationPlanSerializable ==null)
            return "";
        else
        {
            Gson gson = new Gson();
            System.out.println("Medication plan has been read by client");
            return gson.toJson(medicationPlanSerializable);
        }

    }

    @Override
    public String medicationTaken(boolean taken, String medicationPlanId) {
        if(taken) {
            System.out.println("Medication with id: " + medicationPlanId + " has been taken");
            return "medication taken";
        }
        else {
            System.out.println("Medication with id: " + medicationPlanId + " hasn't been taken");
            return "medication not taken";
        }
    }

}
