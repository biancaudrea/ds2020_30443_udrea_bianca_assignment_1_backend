package ro.tuc.ds2020.rmi;

public interface MedicationPlanServiceRmi {
    String getMedicationPlan(String patientId) throws Exception;
    String medicationTaken(boolean taken,  String medicationPlanId);
}
