package ro.tuc.ds2020.rmi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Server {

    private final MedicationPlanServiceImpl medicationPlanServiceImpl;

    @Autowired
    public Server(MedicationPlanServiceImpl medicationPlanServiceImpl){
        this.medicationPlanServiceImpl = medicationPlanServiceImpl;
    }

    @Bean(name = "/booking")
    HttpInvokerServiceExporter accountService() {
        HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
        exporter.setService( medicationPlanServiceImpl);
        exporter.setServiceInterface( MedicationPlanServiceRmi.class );
        return exporter;
    }


}
