package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Patient;

import javax.transaction.Transactional;
import java.util.UUID;

@Repository
public interface PatientRepository extends JpaRepository<Patient, String> {
    Patient findByUsername(String username);
    Patient findByName(String name);

    @Transactional
    String deleteByName(String name);
}
