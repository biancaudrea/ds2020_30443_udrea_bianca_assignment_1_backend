package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.SideEffect;

import javax.transaction.Transactional;

@Repository
public interface SideEffectRepository  extends JpaRepository<SideEffect, String> {
    @Transactional
    String deleteByName(String name);

    @Transactional
    String deleteByMedication(Medication medication);
}
