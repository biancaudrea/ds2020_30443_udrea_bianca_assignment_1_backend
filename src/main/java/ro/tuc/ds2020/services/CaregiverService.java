package ro.tuc.ds2020.services;

import org.hibernate.sql.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public Caregiver findById(String id){return caregiverRepository.findById(id).get();}

    public Caregiver findByName(String name){return caregiverRepository.findByName(name);}

    public void saveCaregiver(Caregiver caregiver){caregiverRepository.save(caregiver);}

    public void addPatientToCaregiver(Patient patient,Caregiver caregiver){
        caregiver.addPatient(patient);
        caregiverRepository.save(caregiver);
    }

    public String insert(CaregiverDetailsDto caregiverDetailsDto, Doctor doctor) {
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDetailsDto,doctor);
        caregiver=caregiverRepository.save(caregiver);
        doctorService.addCaregiverToDoctor(caregiver,doctor);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public String updateAddress(CaregiverUpdateDto caregiverUpdateDto)
    {
        Caregiver caregiver = findByName(caregiverUpdateDto.getName());
        caregiver.setAddress(caregiverUpdateDto.getAddress());
        caregiverRepository.save(caregiver);
        return caregiver.getId();
    }
    public String deleteCaregiver(DeleteCaregiverDto deleteCaregiverDto)
    {
        Caregiver caregiver=findByName(deleteCaregiverDto.getName());
        Doctor doctor=caregiver.getDoctor();
        doctor.removeCaregiver(caregiver);
        doctorService.saveDoctor(doctor);
        String id=caregiver.getId();
        caregiverRepository.deleteByName(deleteCaregiverDto.getName());
        return id;
    }

    public Caregiver findByUsername(String username){return caregiverRepository.findByUsername(username);}

    public List<PatientDto> getAllPatients(String username){

        return caregiverRepository.findByUsername(username).getPatients().stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }
}
