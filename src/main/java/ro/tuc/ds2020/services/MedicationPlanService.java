package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationPlanDto;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.rmi.MedicationPlanSerializable;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    private PatientService patientService;

    @Autowired
    private MedicationService medicationService;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository){this.medicationPlanRepository=medicationPlanRepository;}


    public List<MedicationPlanDto> getAllMedicationPlanDtos(){
        return medicationPlanRepository.findAll().stream().map(MedicationPlanBuilder::toMedicationPlanDto).collect(Collectors.toList());

    }

    public String insert(MedicationPlanDto medicationPlanDto){
        Patient patient=patientService.findByName(medicationPlanDto.getPatientName());
        Medication medication=medicationService.findByName(medicationPlanDto.getMedicationName());
        MedicationPlan medicationPlan=MedicationPlanBuilder.toEntity(medicationPlanDto,patient,medication);
        medicationPlanRepository.save(medicationPlan);
        patient.addMedicationPlan(medicationPlan);
        patientService.savePatient(patient);
        medication.addMedicationPlan(medicationPlan);
        medicationService.saveMedication(medication);
        return medicationPlan.getId();
    }

    //for rmi service
    public MedicationPlanSerializable getMedicationPlanForPatient(String patientId){
        for(MedicationPlan medicationPlan: medicationPlanRepository.findAll())
        {
            if(medicationPlan.getPatient().getUsername().equals(patientId)){
                return MedicationPlanBuilder.toMedicationPlanSerializable(medicationPlan);
            }

        }
        return null;

    }

}
