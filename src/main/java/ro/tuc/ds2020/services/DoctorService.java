package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.CaregiverDto;
import ro.tuc.ds2020.dtos.PatientDto;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.DoctorRepository;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public Doctor findById(String id){return doctorRepository.findById(id).get();}

    public List<Caregiver> getAllCaregivers(String username){

        return doctorRepository.findByUsername(username).getCaregivers();}

    public List<PatientDto> getAllPatients(String username){
        List<Patient> allPatients = new ArrayList<>();

        for(Caregiver caregiver:getAllCaregivers(username))
        {
            allPatients.addAll(caregiver.getPatients());
        }

        return allPatients.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }
    public Doctor findByUsername(String username){return doctorRepository.findByUsername(username);}

    public void saveDoctor(Doctor doctor){doctorRepository.save(doctor);}

    public void addCaregiverToDoctor(Caregiver caregiver,Doctor doctor){
        doctor.addCaregiver(caregiver);
        doctorRepository.save(doctor);
    }
    public List<CaregiverDto> getAllCaregiversOfDoctor(Doctor doctor)
    {
        return doctor.getCaregivers().stream().map(CaregiverBuilder::toCaregiverDTO).collect(Collectors.toList());
    }
}
