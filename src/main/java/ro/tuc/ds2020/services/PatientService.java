package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private CaregiverService caregiverService;


    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<PatientDto> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public PatientDetailsDto findPatientById(String id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPersonDetailsDTO(prosumerOptional.get());
    }

    public Patient findById(String id){return patientRepository.findById(id).get();}

    public void savePatient(Patient patient){patientRepository.save(patient);}

    public Patient findByUsername(String username){return patientRepository.findByUsername(username);}

    public Patient findByName(String name){return patientRepository.findByName(name);}

    public String insert(PatientDetailsDto patientDetailsDto, Caregiver caregiver) {
        Patient patient=PatientBuilder.toEntity(patientDetailsDto,caregiver);
        patient=patientRepository.save(patient);
        caregiverService.addPatientToCaregiver(patient,caregiver);
        LOGGER.debug("Person with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public String updateMedicalRecord(PatientUpdateDto patientUpdateDto)
    {
        Patient patient=findByName(patientUpdateDto.getName());
        patient.setMedicalRecord(patientUpdateDto.getMedicalRecord());
        patientRepository.save(patient);
        return patient.getId();
    }

    public String deletePatient(DeletePatientDto deletePatientDto)
    {
        Patient patient=findByName(deletePatientDto.getName());
        Caregiver caregiver=patient.getCaregiver();
        caregiver.removePatient(patient);
        caregiverService.saveCaregiver(caregiver);
        String id=patient.getId();
        patientRepository.deleteByName(deletePatientDto.getName());
        return id;
    }

    public List<PatientInfoDto> getPatient(String username){
        List<Patient> list = new ArrayList<>();

       list.add(patientRepository.findByUsername(username));

        return list.stream()
                .map(PatientBuilder::toPersonInfoDto)
                .collect(Collectors.toList());
    }

    public List<MedPlanDto> getAllMedPlanDtos(String username){
        return findByUsername(username).getMedicationPlanList().stream().map(MedicationPlanBuilder::toMedPlanDto).collect(Collectors.toList());

    }
}
