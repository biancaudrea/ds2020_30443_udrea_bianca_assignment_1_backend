package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.consumer.CustomMessage;
import ro.tuc.ds2020.entities.ActivityData;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.ActivityDataRepository;

@Service
public class ActivityDataService {
    @Autowired
    private ActivityDataRepository activityDataRepository;

    @Autowired
    private PatientService patientService;

    public void insertActivityData(ActivityData activityData){
        activityDataRepository.save(activityData);
    }

    public void insertActivityData(CustomMessage customMessage){
        Patient patient = patientService.findByUsername(customMessage.getPatientId());
        ActivityData activityData;
        if(patient!=null) {
            activityData = new ActivityData(patient, customMessage.getStartTime(), customMessage.getEndTime(), customMessage.getActivityLabel());
            activityDataRepository.save(activityData);
            patient.addActivity(activityData);
            patientService.savePatient(patient);
        }
    }
}
