package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.DeleteMedicationDto;
import ro.tuc.ds2020.dtos.MedicationDetailsDto;
import ro.tuc.ds2020.dtos.MedicationDto;
import ro.tuc.ds2020.dtos.MedicationUpdateDto;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.SideEffect;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private final MedicationRepository medicationRepository;

    @Autowired
    private SideEffectService sideEffectService;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public List<MedicationDto> getAllMedication() {
        return medicationRepository.findAll().stream().map(MedicationBuilder::toMedicationDto).collect(Collectors.toList());
    }

    public void saveMedication(Medication medication){
        medicationRepository.save(medication);
    }

    public String insert(MedicationDetailsDto medicationDetailsDto) {
        SideEffect sideEffect = new SideEffect();
        sideEffect.setName(medicationDetailsDto.getSideEffects());
        sideEffectService.insertSideEffect(sideEffect);
        Medication medication = MedicationBuilder.toEntity(medicationDetailsDto, sideEffect);
        medicationRepository.save(medication);
        sideEffect.setMedication(medication);
        sideEffectService.insertSideEffect(sideEffect);

        return medication.getId();
    }

    public Medication findByName(String name) {
        return medicationRepository.findByName(name);
    }

    public String updateMedication(MedicationUpdateDto medicationUpdateDto) {
        Medication medication = findByName(medicationUpdateDto.getName());
        SideEffect sideEffect = new SideEffect(medicationUpdateDto.getSideEffects(), medication);
        sideEffectService.insertSideEffect(sideEffect);
        medication.addSideEffect(sideEffect);
        medicationRepository.save(medication);
        return medication.getId();
    }

    public String deleteMedication(DeleteMedicationDto deleteMedicationDto) {
        Medication medication = medicationRepository.findByName(deleteMedicationDto.getName());
        List<SideEffect> sideEffects = medication.getSideEffects();
        int iterations=sideEffects.size();
        for(int i=0;i<iterations;i++)
        {
            medication.removeSideEffect(sideEffects.get(i));
            medicationRepository.save(medication);
        }
//        for (SideEffect sideEffect : sideEffects) {
//            medication.removeSideEffect(sideEffect);
//            medicationRepository.save(medication);
//        }


//        Iterator<SideEffect> itr = sideEffects.iterator(); // remove all even numbers
//        while (itr.hasNext()) {
//            SideEffect sideEffect = itr.next();
//            {
//                medication.removeSideEffect(sideEffect);
//                medicationRepository.save(medication);
//            }
//        }

        sideEffectService.deleteSideEffectByMedication(medication);

        medicationRepository.deleteByName(deleteMedicationDto.getName());
        return medication.getId();
    }
}
