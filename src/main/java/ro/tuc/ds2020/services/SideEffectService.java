package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.SideEffect;
import ro.tuc.ds2020.repositories.SideEffectRepository;

@Service
public class SideEffectService {
    private final SideEffectRepository sideEffectRepository;

    @Autowired
    public SideEffectService(SideEffectRepository sideEffectRepository) {
        this.sideEffectRepository = sideEffectRepository;
    }

    public void insertSideEffect(SideEffect sideEffect){
        sideEffectRepository.save(sideEffect);
    }

    public void deleteSideEffect(SideEffect sideEffect){
        sideEffectRepository.delete(sideEffect);
    }

    public void deleteSideEffectByMedication(Medication medication){
        sideEffectRepository.deleteByMedication(medication);
    }

}
