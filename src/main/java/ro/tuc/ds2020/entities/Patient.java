package ro.tuc.ds2020.entities;

import ro.tuc.ds2020.enums.GenderEnum;
import ro.tuc.ds2020.enums.UserRole;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
public class Patient implements Serializable {


    @Id
    private String id= UUID.randomUUID().toString();

    @Column
    private String medicalRecord;

    @ManyToOne
    private Caregiver caregiver;

    private static final long serialVersionUID = 1L;


    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "age", nullable = false)
    private int age;

    @Column
    private String birthDate;

    @Column
    private GenderEnum gender;

    @OneToMany
    private List<MedicationPlan> medicationPlanList;

    @OneToMany
    private List<ActivityData> activityDataList;

    public Patient() {
    }

    public Patient(String name, String address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }

    @Column
    private String username;

    @Column
    private String password;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }



    public Patient(String medicalRecord, Caregiver caregiver, String name, String address, String birthDate, GenderEnum gender,String username) {
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.gender = gender;
        this.medicationPlanList = new ArrayList<>();
        this.username = username;
        this.password = username;
        this.role = UserRole.PATIENT;
        System.out.println(birthDate);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate dateOfBirthDateFormat = LocalDate.parse(birthDate, formatter);
        Period p=Period.between(dateOfBirthDateFormat,LocalDate.now());
        this.age=p.getYears();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    /**
     * All this fields are from UserDetails.
     */
    // @Column( nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public List<MedicationPlan> getMedicationPlanList() {
        return medicationPlanList;
    }

    public void setMedicationPlanList(List<MedicationPlan> medicationPlanList) {
        this.medicationPlanList = medicationPlanList;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public void addMedicationPlan(MedicationPlan medicationPlan){
        this.medicationPlanList.add(medicationPlan);
    }

    public void addActivity(ActivityData activityData){
        if(activityDataList == null)
        {
            this.activityDataList = new ArrayList<>();
        }
        activityDataList.add(activityData);
    }
}
