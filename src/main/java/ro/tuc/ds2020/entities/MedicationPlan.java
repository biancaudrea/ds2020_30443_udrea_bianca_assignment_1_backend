package ro.tuc.ds2020.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
public class MedicationPlan {

    @Id
    private String id= UUID.randomUUID().toString();

    @ManyToOne
    private Patient patient;

    @ManyToOne
    private Medication medication;

    //no of hours
    @Column
    private int intakeIntervals;

    //no of days
    @Column
    private int period;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public int getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(int intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public MedicationPlan() {
    }

    public MedicationPlan(Patient patient, Medication medication, int intakeIntervals, int period) {
        this.patient = patient;
        this.medication = medication;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
    }
}
