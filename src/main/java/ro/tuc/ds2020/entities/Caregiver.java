package ro.tuc.ds2020.entities;
import ro.tuc.ds2020.enums.GenderEnum;
import ro.tuc.ds2020.enums.UserRole;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
public class Caregiver implements Serializable {
    @Id
    private String id= UUID.randomUUID().toString();


    @OneToMany
    private List<Patient> patients;

    private static final long serialVersionUID = 1L;


    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "age", nullable = false)
    private int age;

    @Column
    private String birthDate;

    @ManyToOne
    private Doctor doctor;

    @Column
    private GenderEnum gender;

    public Caregiver() {
    }

    public Caregiver(String name, String address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }

    @Column
    private String username;

    @Column
    private String password;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    /**
     * All this fields are from UserDetails.
     */
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;

    public Caregiver( String name, String address, String birthDate, Doctor doctor, GenderEnum gender,String username) {
        this.patients = new ArrayList<>();
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.doctor = doctor;
        this.gender = gender;
        this.username = username;
        this.password = username;
        this.role = UserRole.CAREGIVER;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate dateOfBirthDateFormat = LocalDate.parse(birthDate, formatter);
        Period p=Period.between(dateOfBirthDateFormat,LocalDate.now());
        this.age=p.getYears();
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public void addPatient(Patient patient){this.patients.add(patient);}

    public void removePatient(Patient patient){
        for(Patient patient1:patients)
        {
            if(patient1.getId().equals(patient.getId())) {
                patients.remove(patient1);
                break;
            }

        }
    }
}
