package ro.tuc.ds2020.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Entity
public class Medication {

    @Id
    private String id= UUID.randomUUID().toString();

    @Column
    private String name;

    @OneToMany
    private List<SideEffect> sideEffects;

    @Column
    private long dosage;

    @OneToMany
    private List<MedicationPlan> medicationPlanList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SideEffect> getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(List<SideEffect> sideEffects) {
        this.sideEffects = sideEffects;
    }

    public long getDosage() {
        return dosage;
    }

    public void setDosage(long dosage) {
        this.dosage = dosage;
    }

    public List<MedicationPlan> getMedicationPlanList() {
        return medicationPlanList;
    }

    public void setMedicationPlanList(List<MedicationPlan> medicationPlanList) {
        this.medicationPlanList = medicationPlanList;
    }

    public Medication(String name, SideEffect sideEffect, long dosage) {
        this.name = name;
        this.sideEffects = new ArrayList<>();
        sideEffects.add(sideEffect);
        this.dosage = dosage;
        this.medicationPlanList = new ArrayList<>();
    }

    public Medication() {
    }

    public void addSideEffect(SideEffect sideEffect){
        this.sideEffects.add(sideEffect);
    }

    public void removeSideEffect(SideEffect sideEffect){
        for(SideEffect sideEffect1:sideEffects)
        {
            if(sideEffect1.getId().equals(sideEffect.getId())) {
                sideEffects.remove(sideEffect1);
                break;
            }

        }
    }

    public void addMedicationPlan(MedicationPlan medicationPlan){
        this.medicationPlanList.add(medicationPlan);
    }
}
