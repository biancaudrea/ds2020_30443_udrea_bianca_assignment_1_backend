package ro.tuc.ds2020.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class SideEffect implements Serializable {
    @Id
    private String id= UUID.randomUUID().toString();

    @Column
    private String name;

    @ManyToOne
    private Medication medication;

    public SideEffect( String name, Medication medication) {
        this.name = name;
        this.medication = medication;
    }

    public SideEffect() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }
}

