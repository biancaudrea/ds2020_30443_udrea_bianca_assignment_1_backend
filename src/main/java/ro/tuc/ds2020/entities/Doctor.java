package ro.tuc.ds2020.entities;

import ro.tuc.ds2020.enums.GenderEnum;
import ro.tuc.ds2020.enums.UserRole;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
public class Doctor implements Serializable {
    @Id
    private String id= UUID.randomUUID().toString();

    private static final long serialVersionUID = 1L;


    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "age", nullable = false)
    private int age;

    @Column
    private String birthDate;

    @Column
    private GenderEnum gender;


    @OneToMany
    private List<Caregiver> caregivers;

    public Doctor() {
    }

    public Doctor(String name, String address, int age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }

    @Column
    private String username;

    @Column
    private String password;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public Doctor(String name, String address, String birthDate, GenderEnum gender,String username) {
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.gender = gender;
        this.caregivers = new ArrayList<>();
        this.username = username;
        this.password = username;
        this.role = UserRole.DOCTOR;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate dateOfBirthDateFormat = LocalDate.parse(birthDate, formatter);
        Period p=Period.between(dateOfBirthDateFormat,LocalDate.now());
        this.age=p.getYears();
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    /**
     * All this fields are from UserDetails.
     */
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public List<Caregiver> getCaregivers() {
        return caregivers;
    }

    public void setCaregivers(List<Caregiver> caregivers) {
        this.caregivers = caregivers;
    }

    public void addCaregiver(Caregiver caregiver){this.caregivers.add(caregiver);}

    public void removeCaregiver(Caregiver caregiver){
        for(Caregiver caregiver1:caregivers)
        {
            if(caregiver1.getId().equals(caregiver.getId())) {
                caregivers.remove(caregiver1);
                break;
            }

        }
    }
}
