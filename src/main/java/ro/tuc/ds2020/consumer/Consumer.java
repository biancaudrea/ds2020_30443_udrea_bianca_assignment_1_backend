package ro.tuc.ds2020.consumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.ActivityDataService;
import ro.tuc.ds2020.consumer.CustomMessage;
import ro.tuc.ds2020.services.PatientService;

import java.time.*;

@Component
public class Consumer implements MessageListener {

    @Autowired
    private ActivityDataService activityDataService;

    @Autowired
    private PatientService patientService;

//    @RabbitListener(queues = "${rabbitmq.queue}")
//    public void receiveMessage(final CustomMessage customMessage)  {
//        System.out.println("Received message as specific class: {}" + customMessage.toString());
//        // activityDataService.insertActivityData(customMessage);
//    }

    @Override
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message) {
        //System.out.println(new String(message.getBody()));
        try {
            CustomMessage customMessage = new ObjectMapper().readValue(new String(message.getBody()), CustomMessage.class);
            System.out.println(customMessage);
            checkRulesForJson(customMessage);
            activityDataService.insertActivityData(customMessage);
        } catch (Exception e) {
            System.out.println("exception mapping json object" + e.getStackTrace());
        }

    }

    public void checkRulesForJson(CustomMessage customMessage) {
        LocalDateTime startDate =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(customMessage.getStartTime()), ZoneId.systemDefault());
        LocalDateTime endDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(customMessage.getEndTime()), ZoneId.systemDefault());
        String activity = customMessage.getActivityLabel();

        //check for rule 1: Sleep period longer than 7 hours
        Duration duration = Duration.between(startDate, endDate);
        if (duration.toHours() > 7 && activity.contains("Sleeping"))
            alertCaregiver(customMessage, duration.toHours() + " hours");

        //check for rule 2: The leaving activity (outdoor) is longer than 5 hours
        if (duration.toHours() > 5 && activity.contains("Leaving"))
            alertCaregiver(customMessage, duration.toHours() + " hours");

        //check for rule 3: Period spent in bathroom is longer than 30 minutes
        if (duration.toMinutes() > 30 && activity.contains("Toileting"))
            alertCaregiver(customMessage, duration.toMinutes() + " minutes");
    }

    public void alertCaregiver(CustomMessage customMessage, String duration) {
        Patient patient = patientService.findById(customMessage.getPatientId());
        Caregiver caregiver = patient.getCaregiver();

        System.out.println("!!! Alert caregiver: " + caregiver.getName() + " that her patient: " + patient.getName() + " has the following alarming activity: " + customMessage.getActivityLabel() + " for: " + duration);
    }


}