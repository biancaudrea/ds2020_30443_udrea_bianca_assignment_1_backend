package ro.tuc.ds2020.dtos;

public class DeletePatientDto {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeletePatientDto(String name) {
        this.name = name;
    }

    public DeletePatientDto() {
    }
}
