package ro.tuc.ds2020.dtos;

public class DeleteCaregiverDto {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeleteCaregiverDto(String name) {
        this.name = name;
    }

    public DeleteCaregiverDto() {
    }
}
