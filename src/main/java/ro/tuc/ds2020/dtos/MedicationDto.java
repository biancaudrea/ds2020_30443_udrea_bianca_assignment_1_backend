package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.SideEffect;

import java.util.List;

public class MedicationDto {
    private String id;
    private String name;
    private long dosage;
    private String sideEffects;

    public MedicationDto(String id, String name, long dosage, List<SideEffect> sideEffects) {
        this.id = id;
        this.name = name;
        this.dosage = dosage;
        String string="";
        for(SideEffect sideEffect:sideEffects){
           string+=sideEffect.getName()+"; ";
        }
        this.sideEffects = string;
    }

    public MedicationDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDosage() {
        return dosage;
    }

    public void setDosage(long dosage) {
        this.dosage = dosage;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
