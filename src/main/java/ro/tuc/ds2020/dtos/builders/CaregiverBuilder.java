package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDetailsDto;
import ro.tuc.ds2020.dtos.CaregiverDto;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.enums.GenderEnum;

public class CaregiverBuilder {
    public static CaregiverDto toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDto(caregiver.getId(),caregiver.getName(),caregiver.getAge(),caregiver.getAddress());
    }

    public static CaregiverDetailsDto toCaregiverDetailsDto(Caregiver caregiver) {
        return new CaregiverDetailsDto(caregiver.getName(),caregiver.getAddress(),caregiver.getBirthDate(),caregiver.getGender().toString(),caregiver.getUsername());
    }

    public static Caregiver toEntity(CaregiverDetailsDto caregiverDetailsDto, Doctor doctor) {

        return new Caregiver(caregiverDetailsDto.getName(),caregiverDetailsDto.getAddress(),caregiverDetailsDto.getBirthDate(),doctor, GenderEnum.valueOf(caregiverDetailsDto.getGender()),caregiverDetailsDto.getUsername());
    }
}
