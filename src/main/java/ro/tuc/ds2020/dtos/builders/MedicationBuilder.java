package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDetailsDto;
import ro.tuc.ds2020.dtos.MedicationDto;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.SideEffect;

public class MedicationBuilder {
    public static MedicationDto toMedicationDto(Medication medication) {
        return new MedicationDto(medication.getId(), medication.getName(),medication.getDosage(), medication.getSideEffects());
    }

//    public static PatientDetailsDto toPersonDetailsDTO(Patient patient) {
//        return new PatientDetailsDto(patient.getName(), patient.getMedicalRecord(), patient.getCaregiver().getName(), patient.getAddress(), patient.getBirthDate(), patient.getGender().toString(),patient.getUsername());
//    }
//
    public static Medication toEntity(MedicationDetailsDto medicationDetailsDto, SideEffect sideEffect) {

        return new Medication(medicationDetailsDto.getName(),sideEffect,medicationDetailsDto.getDosage());
    }

}
