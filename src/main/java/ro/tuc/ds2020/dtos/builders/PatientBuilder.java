package ro.tuc.ds2020.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.PatientDetailsDto;
import ro.tuc.ds2020.dtos.PatientDto;
import ro.tuc.ds2020.dtos.PatientInfoDto;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.enums.GenderEnum;
import ro.tuc.ds2020.services.CaregiverService;

public class PatientBuilder {

    public static PatientDto toPatientDTO(Patient patient) {
        return new PatientDto(patient.getId(), patient.getName(), patient.getAge(), patient.getMedicalRecord());
    }

    public static PatientDetailsDto toPersonDetailsDTO(Patient patient) {
        return new PatientDetailsDto(patient.getName(), patient.getMedicalRecord(), patient.getCaregiver().getName(), patient.getAddress(), patient.getBirthDate(), patient.getGender().toString(),patient.getUsername());
    }

    public static PatientInfoDto toPersonInfoDto(Patient patient) {
        return new PatientInfoDto(patient.getName(),patient.getUsername(),patient.getAddress(),String.valueOf(patient.getGender()),patient.getAge(),patient.getBirthDate(),patient.getMedicalRecord());
    }

    public static Patient toEntity(PatientDetailsDto patientDetailsDto,Caregiver caregiver) {

        return new Patient(patientDetailsDto.getMedicalRecord(),caregiver,patientDetailsDto.getName(),patientDetailsDto.getAddress(),patientDetailsDto.getBirthDate(), GenderEnum.valueOf(patientDetailsDto.getGender()),patientDetailsDto.getUsername());
    }

}