package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedPlanDto;
import ro.tuc.ds2020.dtos.MedicationPlanDto;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.rmi.MedicationPlanSerializable;

public class MedicationPlanBuilder {
    public static MedicationPlanDto toMedicationPlanDto(MedicationPlan medicationPlan) {
        return new MedicationPlanDto(medicationPlan.getPatient().getName(), medicationPlan.getMedication().getName(),medicationPlan.getIntakeIntervals(), medicationPlan.getPeriod());
    }

    public static MedicationPlan toEntity(MedicationPlanDto medicationPlanDto, Patient patient, Medication medication) {

        return new MedicationPlan(patient,medication,medicationPlanDto.getIntakeIntervals(),medicationPlanDto.getPeriod());
    }
    public static MedPlanDto toMedPlanDto(MedicationPlan medicationPlan) {
        return new MedPlanDto(medicationPlan.getMedication().getName(),medicationPlan.getIntakeIntervals(), medicationPlan.getPeriod());
    }

    public static MedicationPlanSerializable toMedicationPlanSerializable(MedicationPlan medicationPlan){
        return new MedicationPlanSerializable(medicationPlan.getId(),medicationPlan.getMedication().getName(),medicationPlan.getIntakeIntervals(),medicationPlan.getPeriod());
    }

}
