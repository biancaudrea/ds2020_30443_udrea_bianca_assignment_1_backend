package ro.tuc.ds2020.dtos;

public class CaregiverDetailsDto {

    private String name;

    private String address;

    private String birthDate;

    private String gender;

    private String username;

    public CaregiverDetailsDto(String name, String address, String birthDate, String gender, String username) {
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.gender = gender;
        this.username = username;
    }

    public CaregiverDetailsDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
