package ro.tuc.ds2020.dtos;

public class MedicationDetailsDto {
    private String name;
    private long dosage;
    private String sideEffects;

    public MedicationDetailsDto() {
    }

    public MedicationDetailsDto(String name, long dosage, String sideEffects) {
        this.name = name;
        this.dosage = dosage;
        this.sideEffects = sideEffects;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDosage() {
        return dosage;
    }

    public void setDosage(long dosage) {
        this.dosage = dosage;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }
}
