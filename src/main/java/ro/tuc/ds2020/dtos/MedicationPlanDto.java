package ro.tuc.ds2020.dtos;

public class MedicationPlanDto {
    private String patientName;
    private String medicationName;
    private int intakeIntervals;
    private int period;

    public MedicationPlanDto() {
    }

    public MedicationPlanDto(String patientName, String medicationName, int intakeIntervals, int period) {
        this.patientName = patientName;
        this.medicationName = medicationName;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public int getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(int intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
