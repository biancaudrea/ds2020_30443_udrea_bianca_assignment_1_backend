package ro.tuc.ds2020.dtos;

public class PatientInfoDto {
    private String name;
    private String username;
    private String address;
    private String gender;
    private int age;
    private String birthday;
    private String medicalRecord;

    public PatientInfoDto(String name, String username, String address, String gender, int age, String birthday, String medicalRecord) {
        this.name = name;
        this.username = username;
        this.address = address;
        this.gender = gender;
        this.age = age;
        this.birthday = birthday;
        this.medicalRecord = medicalRecord;
    }

    public PatientInfoDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
