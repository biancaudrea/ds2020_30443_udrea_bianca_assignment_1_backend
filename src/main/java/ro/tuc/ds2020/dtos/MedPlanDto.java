package ro.tuc.ds2020.dtos;

public class MedPlanDto {
    private String medicationName;
    private int intakeIntervals;
    private int period;

    public MedPlanDto() {
    }

    public MedPlanDto(String medicationName, int intakeIntervals, int period) {
        this.medicationName = medicationName;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public int getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(int intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
