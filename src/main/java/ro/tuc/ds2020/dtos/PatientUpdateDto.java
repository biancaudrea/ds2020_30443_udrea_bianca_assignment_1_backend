package ro.tuc.ds2020.dtos;

public class PatientUpdateDto {
    private String name;
    private String medicalRecord;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public PatientUpdateDto(String name, String medicalRecord) {
        this.name = name;
        this.medicalRecord = medicalRecord;
    }

    public PatientUpdateDto() {
    }
}
