package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;

public class PatientDetailsDto {

    @NotNull
    private String name;

    private String medicalRecord;

    private String caregiver;

    private String address;

    private String birthDate;

    private String gender;

    private String username;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(String caregiverName) {
        this.caregiver = caregiverName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public PatientDetailsDto(String name, String medicalRecord, String caregiver, String address, String birthDate, String gender,String username) {
        this.name = name;
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
        this.address = address;
        this.birthDate = birthDate;
        this.gender = gender;
        this.username=username;
    }

    public PatientDetailsDto() {
    }
}
