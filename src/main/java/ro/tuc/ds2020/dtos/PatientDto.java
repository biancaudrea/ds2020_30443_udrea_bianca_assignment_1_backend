package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

public class PatientDto extends RepresentationModel<PatientDto> {
    private String id;
    private String name;
    private int age;
    private String medicalRecord;
    public PatientDto() {
    }

    public PatientDto(String id, String name, int age, String medicalRecord) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.medicalRecord=medicalRecord;
    }


    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
