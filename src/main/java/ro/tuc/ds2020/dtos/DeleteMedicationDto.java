package ro.tuc.ds2020.dtos;

public class DeleteMedicationDto {
    private String name;

    public DeleteMedicationDto(String name) {
        this.name = name;
    }

    public DeleteMedicationDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
