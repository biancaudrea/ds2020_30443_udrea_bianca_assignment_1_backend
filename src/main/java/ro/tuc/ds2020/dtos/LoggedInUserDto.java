package ro.tuc.ds2020.dtos;

public class LoggedInUserDto {
    private String username;
    private String id;
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LoggedInUserDto() {
    }

    public LoggedInUserDto(String username, String id, String role) {
        this.role=role;
        this.username = username;
        this.id = id;
    }
}
