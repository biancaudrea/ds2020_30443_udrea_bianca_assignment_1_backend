package ro.tuc.ds2020.dtos;

public class CaregiverUpdateDto {
    private String name;
    private String address;

    public CaregiverUpdateDto(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public CaregiverUpdateDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
