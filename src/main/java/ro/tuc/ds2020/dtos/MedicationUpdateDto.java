package ro.tuc.ds2020.dtos;

public class MedicationUpdateDto {
    private String name;
    private String sideEffects;

    public MedicationUpdateDto() {
    }

    public MedicationUpdateDto(String name, String sideEffects) {
        this.name = name;
        this.sideEffects = sideEffects;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }
}
